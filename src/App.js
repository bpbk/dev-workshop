import React, { Component } from 'react';
import Priorities from './components/Priorities.js'
import './App.css';

const jobData = [
  {
    role : 'Developer',
    priorities : [
      {
        title : 'Income',
        total : 0,
        ratings : []
      },
      {
        title : 'Office Culture',
        total : 0,
        ratings : []
      },
      {
        title : 'Projects',
        total : 0,
        ratings : []
      },
      {
        title : 'Growth Opportunities',
        total : 0,
        ratings : []
      },
      {
        title : 'Job Security / Longevity',
        total : 0,
        ratings : []
      },
      {
        title : 'Creative Freedom',
        total : 0,
        ratings : []
      }
    ]
  },
  {
    role : 'Business Owner',
    priorities : [
      {
        title : 'Dedication',
        total : 0,
        ratings : []
      },
      {
        title : 'Experience',
        total : 0,
        ratings : []
      },
      {
        title : 'Communication',
        total : 0,
        ratings : []
      },
      {
        title : 'Team Player',
        total : 0,
        ratings : []
      },
      {
        title : 'Problem Solving',
        total : 0,
        ratings : []
      },
      {
        title : 'Follow instructions',
        total : 0,
        ratings : []
      }
    ]
  }
]

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: 'Developer'
    }
  }
  render() {
    return (
      <div className="App">
        <div className="toggle">
          <div
            className={this.state.visible === 'Developer' ? 'toggle-item active' : 'toggle-item'}
            onClick={() => this.setState({visible: 'Developer'})}>
            Developer
          </div>
          <div
            className={this.state.visible === 'Business Owner' ? 'toggle-item active' : 'toggle-item'}
            onClick={() => this.setState({visible: 'Business Owner'})}>
            Business Owner
          </div>
        </div>
        {
          jobData.map( data => {
            console.log(data.role)
            return (
              <Priorities visible={this.state.visible === data.role ? true : false} priorities={data.priorities} role={data.role} key={data.role} />
            )
          })
        }
      </div>
    );
  }
}

export default App;
