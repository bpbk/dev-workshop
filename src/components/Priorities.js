import React from 'react'
import Priority from './Priority.js'

export default class Priorities extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      ratings: [],
      rateTotal: 0,
      priorities: this.props.priorities,
      priorityOrder: this.props.priorities
    }
    this.handleAddRating = this.handleAddRating.bind(this)
    this.handleRemoveRating = this.handleRemoveRating.bind(this)
  }
  render () {
    const orderedPriorities = [].concat(this.state.priorities)
    orderedPriorities.sort(function(a, b){
        return a.total-b.total
    })
    return (
      <div className="role-container" style={{display: this.props.visible ? 'block' : 'none'}}>
        <div className="priorities-container">
          <div className="priorities">
            {
              this.state.priorities.map( priority => {
                return (
                  <Priority
                    key={priority.title}
                    priority={priority}
                    addRating={this.handleAddRating}
                    removeRating={this.handleRemoveRating}
                    count={this.props.priorities.length} />
                )
              })
            }
          </div>
          <div className="priority-order">
            {
              orderedPriorities.map ( (priority, ind) => {
                return (
                  <div className="priority-order-item">
                    <span>{ind + 1}.</span>
                    {priority.title}
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    )
  }

  handleAddRating = (priority, rating) => {
    console.log(priority)
    console.log(rating)
    const priorities = this.state.priorities
    const priorityIndex = priorities.indexOf(priority)
    // const newPriority = priorities[priorityIndex]
    // const newPriorityRatings = newPriority.ratings
    priority.ratings.push(rating)
    priorities[priorityIndex].ratings = priority.ratings
    priorities[priorityIndex].total = priorities[priorityIndex].total + rating

    this.setState({
      priorities : priorities
    })
  }

  handleRemoveRating = (priority, rating) => {
    const priorities = this.state.priorities
    const priorityIndex = priorities.indexOf(priority)
    // const newPriority = priorities[priorityIndex]
    // const newPriorityRatings = newPriority.ratings

    priority.ratings.splice( priority.ratings.indexOf(rating), 1 )
    priorities[priorityIndex].ratings = priority.ratings
    priorities[priorityIndex].total = priorities[priorityIndex].total - rating
    this.setState({
      priorities : priorities
    })
  }
}
