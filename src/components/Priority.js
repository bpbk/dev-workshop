import React from 'react'

export default class Priority extends React.Component {
  render () {
    const { priority, count } = this.props
    const ratings = []
    for(let p = 1; p < count + 1; p++ ) {
      ratings.push(
        <div key={p} className="row-count" onClick={() => this.props.addRating(priority, p)}>
          {p}
        </div>
      )
    }
    return (
      <div className="priority">
        <div>
          <div className="row">
            <h3>{priority.title}</h3>
          </div>
          <div className="row row-counts">
            {ratings}
          </div>
          <div className="row ratings-container">
            Ratings:
            <div class="ratings">
              {
                priority.ratings.map( (rating, ind) => {
                  return (
                    <div key={ind} className="active-rating" onClick={() => this.props.removeRating(priority, rating)}>
                      {rating}
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
        <div className="totals">
          total: {priority.total}
        </div>
      </div>
    )
  }
}
